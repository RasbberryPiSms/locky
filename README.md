# MessageToPi

Android client for the project.

Must be used to communicate with the server.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software

```
1. JDK 8
2. Gradle
3. Android with API > 15
4. (Optional) adb  
```

### Building

A step by step series of examples that tell you how to get a development env running

To generate an apk you need to run :

```
gradlew assembleRelease
```
If you want you can [skip the building part and use the  already built apk in the release section](https://gitlab.com/RasbberryPiSms/locky/-/releases)


### Installing
To install the apk on your android phone use

```
adb install app/build/outputs/apk/[release_name].apk
```
Or
```
Install the app directly with the android apk installer (.apk needs to be on the phone) 
```

To use the app in the best conditions, you need to desactivate the notifications on your standard messaging app,
otherwise for each received message on the app you will receive a notification on your other standard messaging app.

We cannot catch the message before the notification beceause it will lock you out of your standard messaging app (default message app)

## Warning

You need to accept all the authorization asked by the app or it will not work at all.

Server must be running to use the app.

## Deployment

If you deploy the app with another server phone number, you can easily

- Change the default server phone number in src/main/java/com/lifprojet/messagetopi/constants/Constants.java
- Ask to the user to change it at the first start

## Built With

* [Gradle](https://gradle.org/)

## Authors

* **Théo REY-VIVIANT**
* **Johan PLANCHON**
* **Johann HUGON**
