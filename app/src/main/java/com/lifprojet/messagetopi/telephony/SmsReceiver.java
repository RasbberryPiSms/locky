package com.lifprojet.messagetopi.telephony;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import androidx.preference.PreferenceManager;

import com.google.common.base.Charsets;
import com.google.common.primitives.Longs;
import com.google.common.primitives.Shorts;
import com.lifprojet.messagetopi.constants.Constants;
import com.lifprojet.messagetopi.sqlite.DBHelper;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class SmsReceiver extends BroadcastReceiver {

    public static final String SMS_BUNDLE = "pdus";

    /**
     * Method triggered when a new message arrive
     * @param context parent context
     * @param intent INTENT_SMS
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Constants.INTENT_SMS)) {
            Bundle intentExtras = intent.getExtras();
            if (intentExtras != null) {
                Object[] pdu_Objects = (Object[]) intentExtras.get(SMS_BUNDLE);
                assert pdu_Objects != null;
                for (Object smsObj : pdu_Objects) {
                    SmsMessage currSms = getMessage(smsObj, intentExtras);
                    if(currSms.getDisplayOriginatingAddress().contains(PreferenceManager.getDefaultSharedPreferences(context).getString(Constants.PREF_SERVER_PHONE_NUMBER, "").substring(1))) {
                        if (currSms.getDisplayMessageBody().substring(0, Math.min(Constants.RSA_REQUEST_HEADER.length(), currSms.getDisplayMessageBody().length())).equals(Constants.RSA_REQUEST_HEADER)){
                            responseRsa(context, currSms.getDisplayMessageBody().substring(Constants.RSA_REQUEST_HEADER.length()));
                        }else{
                            String[] messageParts = currSms.getDisplayMessageBody().split("(?<===)");
                            if(messageParts.length == 2){
                                byte[] plainHeader = Base64.decode(messageParts[0], Base64.DEFAULT);
                                byte[] content = decrypt(context,Base64.decode(messageParts[1], Base64.DEFAULT));
                                if(content != null && plainHeader.length == 25) {
                                    long buildVersion = Longs.fromByteArray(Arrays.copyOfRange(plainHeader, 0, 8));
                                    byte flagFrag = plainHeader[8];
                                    int idSender = Shorts.fromByteArray(Arrays.copyOfRange(content, 0, 2));
                                    int idDest = Shorts.fromByteArray(Arrays.copyOfRange(content, 2, 4));
                                    String messageContent = new String(Arrays.copyOfRange(content, 4, content.length), Charsets.UTF_8);
                                    long fragmentId = 0;
                                    long messageId = 0;
                                    if (flagFrag == Constants.FRAGMENTED_MESSAGE || flagFrag == Constants.END_FRAGMENTED_MESSAGE) {
                                        fragmentId = Longs.fromByteArray(Arrays.copyOfRange(plainHeader, 9, 17));
                                        messageId = Longs.fromByteArray(Arrays.copyOfRange(plainHeader, 17, 25));
                                    }
                                    Log.i("datareceive", "build:" + buildVersion +
                                            " flag:" + flagFrag +
                                            " mId:" + messageId +
                                            " fId:" + fragmentId +
                                            " idSend:" + idSender +
                                            " idDest:" + idDest +
                                            " content:" + messageContent);

                                    if(idSender == Constants.ID_SERVER && idDest == 0) {
                                        String[] args = messageContent.split(Constants.RE_SPLIT_ARGS);
                                        if (args.length > 0)
                                            switch (args[0].split(Constants.RE_SPLIT_DESCR)[0]) {
                                                case Constants.SET_ID:
                                                    if(args.length == 2 && args[0].split(Constants.RE_SPLIT_DESCR).length == 2 && args[1].split(Constants.RE_SPLIT_DESCR).length == 2)
                                                        replyNewId(context,Integer.parseInt(args[0].split(Constants.RE_SPLIT_DESCR)[1]),args[1].split(Constants.RE_SPLIT_DESCR)[1]);
                                                    break;
                                                case Constants.RESPONSE_USER_INFO:
                                                    if (args.length == 1 && (args[0].split(Constants.RE_SPLIT_DESCR)[1]).split(Constants.RE_SPLIT_DATA).length == 3)
                                                        replyUserInfo(context, Integer.parseInt((args[0].split(Constants.RE_SPLIT_DESCR)[1]).split(Constants.RE_SPLIT_DATA)[0]),
                                                                (args[0].split(Constants.RE_SPLIT_DESCR)[1]).split(Constants.RE_SPLIT_DATA)[1],
                                                                (args[0].split(Constants.RE_SPLIT_DESCR)[1]).split(Constants.RE_SPLIT_DATA)[2]);
                                                    break;
                                                case Constants.RESPONSE_ADD_USER_CONV:
                                                    if(args.length == 2 && args[0].split(Constants.RE_SPLIT_DESCR).length == 2 && args[1].split(Constants.RE_SPLIT_DESCR).length == 2)
                                                        replyUserToConv(context, Integer.parseInt(args[0].split(Constants.RE_SPLIT_DESCR)[1]),args[1].split(Constants.RE_SPLIT_DESCR)[1].split(Constants.RE_SPLIT_DATA));
                                                    break;
                                                case Constants.RESPONSE_DEL_USER_CONV:
                                                    if(args.length == 2 && args[0].split(Constants.RE_SPLIT_DESCR).length == 2 && args[1].split(Constants.RE_SPLIT_DESCR).length == 2)
                                                        replyRemoveUserConv(context, Integer.parseInt(args[0].split(Constants.RE_SPLIT_DESCR)[1]),args[1].split(Constants.RE_SPLIT_DESCR)[1].split(Constants.RE_SPLIT_DATA));
                                                    break;
                                                default:
                                                    //TODO
                                            }
                                    } else
                                        receiveStandardMessage(context,messageId,idDest,idSender,fragmentId,flagFrag,messageContent);
                                }
                            }
                        }
                    } else {
                        Toast.makeText(context, "FORWARDED" + currSms.getDisplayMessageBody(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    /**
     * Response at the start when the server gave us our new id
     * @param context Parent context
     * @param idDest idDest
     * @param messageContent messageContent
     */
    private void replyNewId(Context context, int idDest, String messageContent){
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putInt(Constants.PREF_USER_ID , idDest);
        editor.putString(Constants.PREF_CLIENT_PHONE_NUMBER, messageContent);
        editor.apply();
    }

    /**
     * Response from the request User Information
     * @param context Parent context
     * @param idUser userId
     * @param phoneNumber His phone number
     * @param userName His username
     */
    private void replyUserInfo(Context context, int idUser, String phoneNumber, String userName){
        DBHelper database = new DBHelper(context);
        database.addProfile(idUser,phoneNumber,userName);
    }

    /**
     * Add a standard message to the db or
     * handle it if it's a fragment
     * @param context Parent context
     * @param messageId messageId
     * @param idDest idDest
     * @param idSender idSender
     * @param fragmentId fragmentId
     * @param flagFrag flagFrag
     * @param messageContent messageContent
     */
    private void receiveStandardMessage(Context context,long messageId,int idDest, int idSender, long fragmentId, int flagFrag, String messageContent ){
        DBHelper database = new DBHelper(context);
        if(!database.isProfileExist(idSender) && idSender != Constants.ID_SERVER)
            SmsSender.requestUserInfo(context,idSender);
        if(flagFrag == Constants.END_FRAGMENTED_MESSAGE || flagFrag == Constants.FRAGMENTED_MESSAGE){
            if(!database.fragmentHeaderExist(messageId))
                database.createFragmentHeader(messageId, idDest,idSender);
            database.addFragment(messageId, fragmentId, flagFrag == Constants.END_FRAGMENTED_MESSAGE, messageContent);
            database.convertFullFragment(messageId);
        } else {
            database.addMessage(idDest,idSender,messageContent);
        }
    }

    /**
     * Add a list of user to a conversation
     * @param context Parent context
     * @param idConv idConv
     * @param userArray user list to add the conversation
     */
    private void replyUserToConv(Context context, int idConv, String[] userArray){
        DBHelper database = new DBHelper(context);
        for(String userId : userArray) {
            if (!database.isProfileExist(Integer.parseInt(userId)) && Integer.parseInt(userId) != Constants.ID_SERVER)
                SmsSender.requestUserInfo(context, Integer.parseInt(userId));
            database.addUserToConversation(idConv, Integer.parseInt(userId));
        }
    }

    /**
     * Remove a list of user to remove from a conversation
     * @param context Parent context
     * @param idConv conv where user must be remove
     * @param userArray Users to remove from a conv
     */
    private void replyRemoveUserConv(Context context, int idConv, String[] userArray){
        DBHelper database = new DBHelper(context);
        for(String userId : userArray)
            database.removeFromConv(idConv, Integer.parseInt(userId));
    }

    /**
     * Analyse the rsa response
     * @param context Parent context
     * @param key rsaKey responded
     */
    private void responseRsa(Context context, String key){
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(Constants.PREF_RSA_SERVER_PUB , key);
        editor.apply();
        SmsSender.respondRequestSelf(context);
    }

    /**
     * @param context Parent context
     * @param toDecrypt encrypted payload to decrypt
     * @return decrypted payload
     */
    private static byte[] decrypt(Context context, byte[] toDecrypt){
        String rsa_private_key = PreferenceManager.getDefaultSharedPreferences(context).getString(Constants.PREF_RSA_PRIV, "");
        try {
            if(!rsa_private_key.equals("")) {
                byte [] pkcs8EncodedBytes = Base64.decode(rsa_private_key, Base64.DEFAULT);
                PKCS8EncodedKeySpec keySpecPriv = new PKCS8EncodedKeySpec(pkcs8EncodedBytes);
                KeyFactory kf = KeyFactory.getInstance("RSA");
                PrivateKey  privateKey = kf.generatePrivate(keySpecPriv);
                Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                cipher.init(Cipher.DECRYPT_MODE, privateKey);
                return cipher.doFinal(toDecrypt);
            }
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Convert message pdu into SmsMessage object
     * @param aObject sms_pdu
     * @param bundle intent
     * @return SmsMessage
     */
    private SmsMessage getMessage(Object aObject, Bundle bundle) {
        SmsMessage currSMS;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String format = bundle.getString("format");
            currSMS = SmsMessage.createFromPdu((byte[]) aObject, format);
        } else {
            currSMS = SmsMessage.createFromPdu((byte[]) aObject);
        }
        return currSMS;
    }
}