package com.lifprojet.messagetopi.telephony;

import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.SmsManager;
import android.util.Base64;
import android.widget.Toast;

import androidx.preference.PreferenceManager;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Longs;
import com.google.common.primitives.Shorts;
import com.lifprojet.messagetopi.constants.Constants;
import com.lifprojet.messagetopi.obj.MessageFrament;
import com.lifprojet.messagetopi.sqlite.DBHelper;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * Static class to easily send message to the server
 */
public class SmsSender {
    /**
     * Send a message with an encrypted content
     * @param context parent context
     * @param idConv idConversation (dest)
     * @param content What will be sent
     */
    public static void sendSimple(Context context, int idConv, String content) {
        String serverPhone = PreferenceManager.getDefaultSharedPreferences(context).getString(Constants.PREF_SERVER_PHONE_NUMBER, "");
        int senderId = PreferenceManager.getDefaultSharedPreferences(context).getInt(Constants.PREF_USER_ID, 0);
        if(senderId > 0 && !serverPhone.equals("")) {
            DBHelper database = new DBHelper(context);
            //If the message isn't a request store the it in the db
            if(idConv != 0)
                database.addMessage(idConv,senderId, content,true);
            SmsManager smsManager = SmsManager.getDefault();
            long messageId = PreferenceManager.getDefaultSharedPreferences(context).getLong(Constants.PREF_NEXT_MESSAGEID, 1) % Long.MAX_VALUE;
            String rsaServer = PreferenceManager.getDefaultSharedPreferences(context).getString(Constants.PREF_RSA_SERVER_PUB, "");
            List<MessageFrament> fragmentList = generateFragment(content);
            if(fragmentList.size() > 1) {
                SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
                editor.putLong(Constants.PREF_NEXT_MESSAGEID , (messageId + 1));
                editor.apply();
            }
            if(!rsaServer.equals(""))
                for(MessageFrament fragment : fragmentList) {
                    smsManager.sendTextMessage(serverPhone, null,
                            Base64.encodeToString(fragmentList.size() == 1 ? generatePlainHeader() : generatePlainHeader(fragment.isLast(), fragment.getIdFragment(), messageId), Base64.NO_WRAP) +
                                    Base64.encodeToString(encrypt(rsaServer, Bytes.concat(Shorts.toByteArray((short) senderId),Shorts.toByteArray((short)idConv),fragment.getContent().getBytes())), Base64.NO_WRAP)
                            , null, null);
                }
        } else {
            if(senderId == 0)
                Toast.makeText(context, "The userId is not set",
                    Toast.LENGTH_LONG).show();
            else
                Toast.makeText(context, "The server phone number is not set",
                        Toast.LENGTH_LONG).show();
        }
    }

    /**
     *
     * @param context parent Context
     * @param rsa_pub Rsa key to be send
     */
    public static void requestRSA(Context context,String rsa_pub){
        String serverPhone = PreferenceManager.getDefaultSharedPreferences(context).getString(Constants.PREF_SERVER_PHONE_NUMBER, "");
        if(serverPhone.length() > 9) {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(serverPhone, null,Constants.RSA_REQUEST_HEADER + rsa_pub, null,null);
        } else
            Toast.makeText(context, "The server phone number is not set",
                    Toast.LENGTH_LONG).show();
    }

    /**
     * Send your own information to the server (at the start)
     * @param context parent context
     */
    public static void respondRequestSelf(Context context){
        String pub_server_rsa = PreferenceManager.getDefaultSharedPreferences(context).getString(Constants.PREF_RSA_SERVER_PUB,"");
        String serverPhone = PreferenceManager.getDefaultSharedPreferences(context).getString(Constants.PREF_SERVER_PHONE_NUMBER, "");
        String nickname = PreferenceManager.getDefaultSharedPreferences(context).getString(Constants.PREF_NICKNAME,"UNDEF");
        if(!pub_server_rsa.equals("") && serverPhone.length() > 9){
            SmsManager smsManager = SmsManager.getDefault();
            byte[] content = Bytes.concat(Constants.REQUEST_SELF_ID, nickname.getBytes());
            String rsaServer = PreferenceManager.getDefaultSharedPreferences(context).getString(Constants.PREF_RSA_SERVER_PUB, "");
            String message = Base64.encodeToString(generatePlainHeader(), Base64.NO_WRAP) +  Base64.encodeToString(encrypt(rsaServer,content), Base64.NO_WRAP);
            smsManager.sendTextMessage(serverPhone, null, message, null,null);
        } else
            Toast.makeText(context, "The server phone number is not set or issue with the rsa key",
                    Toast.LENGTH_LONG).show();
    }

    /**
     * Generate an header with parameters
     * @param isEnd is it the last Fragment
     * @param idFragment fragment id
     * @param idMessage message id
     * @return header in bytes
     */
    private static byte[] generatePlainHeader(boolean isEnd,long idFragment, long idMessage){
        return Bytes.concat(Longs.toByteArray(Constants.BUILD_VERSION),
                new byte[] {(isEnd ? Constants.END_FRAGMENTED_MESSAGE : Constants.FRAGMENTED_MESSAGE)} ,
                Longs.toByteArray(idFragment),
                Longs.toByteArray(idMessage));
    }

    /**
     * Generate and empty header
     * @return header in bytes
     */
    private static byte[] generatePlainHeader(){
        return Bytes.concat(Longs.toByteArray(Constants.BUILD_VERSION),
                new byte[] {Constants.COMPLETE_MESSAGE} ,
                Longs.toByteArray(0),
                Longs.toByteArray(0));
    }

    /**
     * Split the content into multiple message Fragment
     * @param content content of the message
     * @return list of fragment
     */
    private static List<MessageFrament> generateFragment(String content){
        List<MessageFrament> fragments = new ArrayList<>();
        int fragmentNumber = (int) Math.ceil(content.length() / (double) Constants.CONTENT_SIZE_MESSAGE);
        for(int i = 0; i < fragmentNumber; i++){
            fragments.add(new MessageFrament(i,content.substring(i* Constants.CONTENT_SIZE_MESSAGE, Math.min((i+1)* Constants.CONTENT_SIZE_MESSAGE,content.length())),i == (fragmentNumber - 1)));
        }
        return fragments;
    }

    /**
     * Encrypt the message with the server pub key
     * @param rsaServer rsa server pub key
     * @param toEncrypt byte payload to encrypt
     * @return encrypted payload
     */
    private static byte[] encrypt(String rsaServer, byte[] toEncrypt){
        try {
            byte[] pkcs8EncodedBytesPub = Base64.decode(rsaServer, Base64.DEFAULT);
            X509EncodedKeySpec keySpecPub = new X509EncodedKeySpec(pkcs8EncodedBytesPub);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PublicKey serverPublicKey = kf.generatePublic(keySpecPub);
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, serverPublicKey);
            return cipher.doFinal(toEncrypt);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Request a conversation creation
     * @param context parent Context
     * @param phoneNumber other User phone number
     */
    public static void requestCreateConv(Context context, String phoneNumber){
        sendSimple(context,0,Constants.REQUEST_CREATE_CONVERSATION + Constants.RE_SPLIT_DESCR + "+" + phoneNumber);
    }

    /**
     * Request to add a user to the conversation
     * @param context parent Context
     * @param idConv conversation to add the user
     * @param phoneNumber other User phone number
     */
    public static void requestAddUserConv(Context context,int idConv, String phoneNumber){
        sendSimple(context,0,Constants.REQUEST_ADD_USER_CONV + Constants.RE_SPLIT_DESCR + idConv +
                    Constants.RE_SPLIT_ARGS + Constants.REQUEST_CONV_ID_USER + Constants.RE_SPLIT_DESCR + "+" +  phoneNumber);
    }

    /**
     * Request to leave a conversation
     * @param context parent Context
     * @param idConv id conv to leave
     */
    public static void requestQuitConversation(Context context,int idConv){
        sendSimple(context,0,Constants.REQUEST_QUIT_CONV + Constants.RE_SPLIT_DESCR + idConv);
    }

    /**
     * Request to get the information on another user
     * @param context parent Context
     * @param userId other userId
     */
    public static void requestUserInfo(Context context, int userId){
        sendSimple(context,0, Constants.REQUEST_USER_INFO + Constants.RE_SPLIT_DESCR + userId);
    }
}
