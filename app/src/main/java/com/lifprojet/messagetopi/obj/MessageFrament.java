package com.lifprojet.messagetopi.obj;

/**
 * Message fragment class to handle message fragment like object
 * @author RaspiThim
 */
public class MessageFrament implements Comparable<MessageFrament>{
    private long idMessage;
    private long idFragment;
    private boolean isLast;
    private String content;

    public MessageFrament(long idMessage, long idFragment, String content, boolean isLast){
        this.idMessage = idMessage;
        this.idFragment= idFragment;
        this.content = content;
        this.isLast = isLast;
    }

    public MessageFrament(long idFragment, String content, boolean isLast){
        this.idMessage = 0;
        this.idFragment= idFragment;
        this.content = content;
        this.isLast = isLast;
    }

    public void setIdMessage(int idMessage){this.idMessage = idMessage;}

    public void setIdFragment(int idFragment){this.idMessage = idFragment;}

    public void setContent(int content){this.idMessage = content;}

    public void setLast(int isLast){this.idMessage = isLast;}

    public long getNumberFrag(){
        if(isLast) return idFragment + 1;
        return -1;

    }

    public long getIdMessage() {
        return idMessage;
    }

    public long getIdFragment(){
        return idFragment;
    }

    public boolean isLast(){
        return isLast;
    }

    public String getContent(){
        return content;
    }

    @Override
    public int compareTo(MessageFrament other) {
        return (Long.compare(this.getIdFragment(),other.getIdFragment()));
    }
}