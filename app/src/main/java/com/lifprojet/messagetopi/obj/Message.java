package com.lifprojet.messagetopi.obj;

import java.util.Date;

/**
 * Message class to handle message like object
 * @author RaspiThim
 */
public class Message implements Comparable<Message>{
    private int idMessage;
    private int idConv;
    private int idSender;
    private String nameSender;
    private String phoneNumber;
    private Date date;
    private String content;
    private boolean isGroup;
    private boolean isRead;

    public Message(int idMessage,int idConv, int idSender, String phoneNumber, String nameSender ,Date date, String content, boolean isGroup, boolean isRead){
        this.idMessage = idMessage;
        this.idConv = idConv;
        this.idSender = idSender;
        this.nameSender = (nameSender != null ? nameSender : (phoneNumber != null ? phoneNumber : Integer.toString(idSender)));
        this.phoneNumber = (phoneNumber != null ? phoneNumber : Integer.toString(idSender));
        this.date = date;
        this.content = content;
        this.isGroup = isGroup;
        this.isRead = isRead;
    }

    public Message(int idConv, Date date){
        this.idConv = idConv;
        this.idMessage = 0;
        this.idSender = 0;
        this.date = date;
        this.content = "";
        this.isGroup = false;
        this.isRead = false;
    }

    public void copy(Message other){
        this.idConv = other.getIdConv();
        this.idMessage = other.getIdMessage();
        this.idSender = other.getIdSender();
        this.phoneNumber = other.getPhoneNumber();
        this.nameSender = other.getNameSender();
        this.date = other.getDate();
        this.content = other.getContent();
        this.isGroup = other.isGroup();
        this.isRead = other.isRead();
    }

    public void setIdConv(int idConv){ this.idConv = idConv; }

    public void setIdSender(int idSender){
        this.idSender = idSender;
    }

    public void setDate(Date date){
        this.date = date;
    }

    public void setRead(Boolean isRead){
        this.isRead = isRead;
    }

    public void setGroup(boolean isGroup){
        this.isGroup = isGroup;
    }

    public void setNameSender(String nameSender) { this.nameSender = nameSender; }

    public void setPhoneSender(String phoneNumber) { this.phoneNumber = phoneNumber; }

    public void setContent(String content){
        this.content = content;
    }

    @Override
    public boolean equals(Object obj){
            return (obj instanceof Message &&
                    idMessage == ((Message) obj).getIdMessage());
    }
    public int getIdMessage(){
        return idMessage;
    }

    public int getIdConv(){
        return idConv;
    }

    public int getIdSender(){
        return idSender;
    }

    public Date getDate(){
        return date;
    }

    public String getContent(){
        return content;
    }

    public String getNameSender(){
        return nameSender;
    }

    public boolean isGroup(){
        return isGroup;
    }

    public boolean isRead(){
        return isRead;
    }

    public String getPhoneNumber(){
        return phoneNumber;
    }

    @Override
    public int compareTo(Message o) {
        return this.getDate().compareTo(o.getDate());
    }
}
