package com.lifprojet.messagetopi.constants;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Shorts;

/**
 * Constants file used in the whole app
 * @author RaspiThim
 */
public class Constants {

    //For the database
    public static final String DATABASE_NAME = "smsToPi.db";
    public static final String MESSAGE_TABLE = "message";
    public static final  String ID_CONV = "idConv";
    public static final  String ID_MESSAGE = "idMessage";
    public static final  String ID_SENDER = "idSender";
    public static final  String TIMESTAMP = "datetime";
    public static final  String MESSAGE_CONTENT = "content";
    public static final  String MESSAGE_ISREAD = "read";
    public static final String PROFILE_TABLE = "profile";
    public static final  String NAME_SENDER = "nameSender";
    public static final  String PHONE_SENDER = "phoneNumber";

    public static final String MESSAGE_FRAGMENT_CONTENT_TABLE = "fragmentContent";
    public static final String MESSAGE_FRAGMENT_HEADER_TABLE = "fragmentHeader";
    public static final String FRAGMENT_ISLAST = "isLastFragment";
    public static final String ID_FRAGMENT = "idFragment";

    public static final String USER_CONVERSATION_TABLE = "user_conv_table";

    public static final int MESSAGE_FRAGMENT_TIMEOUT = 1000*60*15;

    //For the intent
    public static final String INTENT_SMS = "android.provider.Telephony.SMS_RECEIVED";

    //For the adapter
    public static final int MESSAGE_DISPLAY_TIME_INTERVAL = 1000*60*15;

    //For StringToColor
    public static final int FAR_COLOR_THRESHOLD = 100;


    //Preference
    public static final String PREF_CLIENT_PHONE_NUMBER = "phone_number_client";
    public static final String PREF_SERVER_PHONE_NUMBER = "phone_number_server";
    public static final String PREF_NICKNAME = "nickname_client";
    public static final String PREF_USER_ID = "user_id";
    public static final String PREF_RSA_PUB = "rsa_pub";
    public static final String PREF_RSA_PRIV = "rsa_priv";
    public static final String PREF_RSA_SERVER_PUB = "rsa_server_pub";
    public static final String PREF_NEXT_MESSAGEID = "message_id";

    //Values
    public static final int BUILD_VERSION = 1;
    public static final int ID_SERVER = 0;
    public static final int RSA_KYE_SIZE = 53;
    public static final int ENCODED_HEADER_SIZE = 4;
    public static final int CONTENT_SIZE_MESSAGE = RSA_KYE_SIZE - ENCODED_HEADER_SIZE;


    //Headers
    public static final String RSA_REQUEST_HEADER = "RSA:";
    public static final byte[] REQUEST_SELF_ID = Bytes.concat(Shorts.toByteArray((short)0),Shorts.toByteArray((short)0));
    public static final byte COMPLETE_MESSAGE = (byte)'N';
    public static final byte FRAGMENTED_MESSAGE = (byte)'F';
    public static final byte END_FRAGMENTED_MESSAGE = (byte)'L';

    public static final String DEFAULT_SERVER_PHONE = "0766495988";

    //Server requests
    public static final String RE_SPLIT_DESCR = ":";
    public static final String RE_SPLIT_ARGS = "!";
    public static final String RE_SPLIT_DATA = ",";

    public static final String REQUEST_CREATE_CONVERSATION = "CREATE_CONV";
    public static final String REQUEST_ADD_USER_CONV = "ADD_USER_CONV";
    public static final String REQUEST_CONV_ID_USER = "ID_USER";
    public static final String REQUEST_QUIT_CONV = "REMOVE_USER_CONV";
    public static final String REQUEST_USER_INFO = "REQUEST_INFO";

    //Server response
    public static final String RESPONSE_DEL_USER_CONV = "REMOVE_USER_CONV";
    public static final String RESPONSE_USER_INFO = "USER_INFO";
    public static final String SET_ID = "SET_ID";
    public static final String RESPONSE_ADD_USER_CONV = "ADD_USER_CONV";

}
