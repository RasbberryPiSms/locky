package com.lifprojet.messagetopi.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.lifprojet.messagetopi.R;
import com.lifprojet.messagetopi.adapter.AdapterClickListener;
import com.lifprojet.messagetopi.adapter.ConversationListAdapter;
import com.lifprojet.messagetopi.constants.Constants;
import com.lifprojet.messagetopi.obj.Message;
import com.lifprojet.messagetopi.sqlite.DBHelper;
import com.lifprojet.messagetopi.telephony.SmsSender;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.List;
import java.util.Objects;

/**
 * Main activity for the app, it contains all the active conversation
 */
public class MainActivity extends AppCompatActivity implements AdapterClickListener {

    private RecyclerView recyclerView;
    private ConversationListAdapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<Message> messList;
    private DBHelper database;
    private MenuItem delete;
    private MenuItem settings;
    private MenuItem wipe; //TODO
    private MenuItem clear;
    private int userId;

    @SuppressLint("HardwareIds")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        database = new DBHelper(this);
        userId = PreferenceManager.getDefaultSharedPreferences(this).getInt(Constants.PREF_USER_ID, 0);
        messList = database.getListConvMessage(userId);
        recyclerView = findViewById(R.id.my_recycler_view);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new ConversationListAdapter(messList,this);
        mAdapter.sort();
        recyclerView.setAdapter(mAdapter);

        /*
         * Button listener to create a new conversation
         */
        findViewById(R.id.but_new_conv).setOnClickListener(v -> {
            @SuppressLint("InflateParams") final RelativeLayout relativeLayout = (RelativeLayout) LayoutInflater.from(v.getContext()).inflate(R.layout.user_toconv_alert_layout, null);
            new MaterialAlertDialogBuilder(v.getContext())
                    .setView(relativeLayout)
                    .setPositiveButton(getResources().getString(R.string.validate_delete), (dialogInterface, i) -> createConv(((EditText) relativeLayout.findViewById(R.id.user_phone)).getText().toString()))
                    .setNegativeButton(getResources().getString(R.string.cancel_delete), (dialogInterface, i) -> {
                    })
                    .show();
        });

        final String[] perms = new String[]{Manifest.permission.SEND_SMS, Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_SMS,Manifest.permission.READ_PHONE_STATE};
        if(!checkIfAlreadyPermission(perms))
            ActivityCompat.requestPermissions(MainActivity.this, perms, 101);
        else startAfterPermission();
    }

    /**
     * Assign object to xml button
     * @param menu override param
     * @return true always
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        this.delete = menu.findItem(R.id.ic_delete);
        this.settings = menu.findItem(R.id.action_settings);
        this.wipe = menu.findItem(R.id.wipe_button);
        this.clear = menu.findItem(R.id.ic_close);
        delete.setVisible(false);
        clear.setVisible(false);
        return true;
    }

    /**
     * What must be done after the first start of the app and the permission
     */
    public void startAfterPermission(){
        String phoneServer = PreferenceManager.getDefaultSharedPreferences(this).getString(Constants.PREF_SERVER_PHONE_NUMBER,"");
        String nickname = PreferenceManager.getDefaultSharedPreferences(this).getString(Constants.PREF_NICKNAME,"");
        if(phoneServer.equals(""))
            this.setDefaultPhoneServer();
        if(nickname.equals(""))
            this.requestInfo();
    }

    /**
     * Set the default server number according to the value in the constants
     */
    public void setDefaultPhoneServer(){
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putString(Constants.PREF_SERVER_PHONE_NUMBER , Constants.DEFAULT_SERVER_PHONE);
        editor.apply();
    }

    /**
     * Generate our rsa key and send request to get the server pub rsa key
     *
     * Send it in pkcs1 format, must be change in the future to x509
     */
    public void fillRsa(){
        KeyPairGenerator keygen;
        try {
            keygen = KeyPairGenerator.getInstance("RSA");
        keygen.initialize(512);
        KeyPair pair = keygen.generateKeyPair();
        PrivateKey privateKey = pair.getPrivate();
        PublicKey publicKey = pair.getPublic();
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putString(Constants.PREF_RSA_PUB , Base64.encodeToString(publicKey.getEncoded(), Base64.NO_WRAP));
        editor.putString(Constants.PREF_RSA_PRIV, Base64.encodeToString(privateKey.getEncoded(), Base64.NO_WRAP));
        editor.apply();
        byte[] pubBytes = publicKey.getEncoded();
        SmsSender.requestRSA(this,Base64.encodeToString(pubBytes, Base64.NO_WRAP));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    /**
     * Ask the user to set his nickname
     */
    public void requestInfo(){
        @SuppressLint("InflateParams") final RelativeLayout relativeLayout = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.request_start, null);
        String phoneServer = PreferenceManager.getDefaultSharedPreferences(this).getString(Constants.PREF_SERVER_PHONE_NUMBER,"");
        ((EditText) relativeLayout.findViewById(R.id.phone_server)).setText(phoneServer);
        new MaterialAlertDialogBuilder(this)
                .setView(relativeLayout)
                .setPositiveButton(getResources().getString(R.string.confirm_information), (dialogInterface, i) -> {
                    if(((EditText) relativeLayout.findViewById(R.id.user_nickname)).getText().toString().replaceAll("\\s+", "").equals("")) {
                        exit();
                    } else {
                        setUserInfo(((EditText) relativeLayout.findViewById(R.id.user_nickname)).getText().toString(),
                                ((EditText) relativeLayout.findViewById(R.id.phone_server)).getText().toString());
                    }
                })
                .show();
    }

    /**
     * Set the nickname of the user into the sharedPreferences
     * @param nickName set nickname
     */
    public void setUserInfo(String nickName,String phoneServer){
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putString(Constants.PREF_NICKNAME, nickName);
        editor.putString(Constants.PREF_SERVER_PHONE_NUMBER , phoneServer);
        editor.apply();
        String pub_server_rsa = PreferenceManager.getDefaultSharedPreferences(this).getString(Constants.PREF_RSA_SERVER_PUB,"");
        if(pub_server_rsa.equals(""))
            this.fillRsa();
    }

    /**
     * Call the create of a conversation if
     * there is any 1v1 conversation with the same user
     * @param phoneNumber phoneNumber of the user we want to create a conversation with
     */
    public void createConv(String phoneNumber){
        for(Message m : messList) {
            Log.i("testPhone", m.getPhoneNumber());
            if (m.getPhoneNumber().contains(phoneNumber) && !m.isGroup()) {
                itemClicked(m);
                return;
            }
        }
        SmsSender.requestCreateConv(this, phoneNumber);
        Toast.makeText(this, "Conversation creation required", Toast.LENGTH_SHORT).show();
    }

    /**
     * Check for the permissions
     * @param perms list of permissions required
     * @return true
     */
    private boolean checkIfAlreadyPermission(String[] perms) {
        for(String i: perms) {
            if (ContextCompat.checkSelfPermission(this, i) != PackageManager.PERMISSION_GRANTED) return false;
        }
        return true;

    }

    /**
     * Permissions result triggered after the user answer to the permission request
     * @param requestCode request code
     * @param permissions permissions list
     * @param grantResults value
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 101) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                this.startAfterPermission();
            } else {
                this.exit();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * Override of the onOptionItemSelected of the menu to be able to use his buttons
     * @param item clicked item
     * @return selected item
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_settings :
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.wipe_button : //TODO
                startActivity(new Intent(this,MainActivity.class));
                PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
                database.clear();
                return true;
            case R.id.ic_delete :
                List<Message> toDelete = mAdapter.getSelectedItem();
                for(Message message : toDelete) {
                    SmsSender.requestQuitConversation(this, message.getIdConv());
                }
                database.delConv(toDelete);
                mAdapter.clearSelection();
                reloadConvList();
                return true;
            case R.id.ic_close :
                mAdapter.clearSelection();
                reloadConvList();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     *  Open the Conversation Activity for the clicked conversation
     * @param m message clicked
     */
    @Override
    public void itemClicked(Message m){
        startActivity(new Intent(this, ConversationActivity.class)
                .putExtra(Constants.ID_CONV,m.getIdConv())
                .putExtra(Constants.NAME_SENDER, m.getNameSender()));
    }

    /**
     * If we are in selection mode (for deleting conversation) we change the button visibility
     * and the background toolbar color
     * @param pending If we are currently in selection
     */
    @Override
    public void selection(boolean pending) {
        delete.setVisible(pending);
        clear.setVisible(pending);
        wipe.setVisible(!pending);
        settings.setVisible(!pending);
        if(pending)
            Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.DarkOrange)));
        else
            Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
    }

    /**
     * Reload the data of the conversationList according to the database state
     * Triggered when we receive a new message from the server number
     */
    public void reloadConvList() {
        database.getListConvMessage(messList, userId);
        mAdapter.sort();
        mAdapter.notifyDataSetChanged();
        recyclerView.post(() -> layoutManager.smoothScrollToPosition(recyclerView, new RecyclerView.State(), 0));
    }

    /**
     * Update the conversation when we reopen de main activity
     */
    @Override
    public void onResume() {
        registerReceiver(mUpdateReceiver, new IntentFilter(Constants.INTENT_SMS));
        reloadConvList();
        super.onResume();
    }

    @Override
    public void onPause() {
        unregisterReceiver(mUpdateReceiver);
        super.onPause();
    }

    /**
     * Call the update when we receive a message
     */
    private BroadcastReceiver mUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            reloadConvList();
        }
    };

    /**
     * Exit the app
     */
    public void exit(){
        finish();
        System.exit(0);
    }

}
