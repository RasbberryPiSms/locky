package com.lifprojet.messagetopi.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.lifprojet.messagetopi.R;
import com.lifprojet.messagetopi.adapter.AdapterClickListener;
import com.lifprojet.messagetopi.adapter.ConversationAdapter;
import com.lifprojet.messagetopi.constants.Constants;
import com.lifprojet.messagetopi.obj.Message;
import com.lifprojet.messagetopi.sqlite.DBHelper;
import com.lifprojet.messagetopi.telephony.SmsSender;

import java.util.List;
import java.util.Objects;

public class ConversationActivity extends AppCompatActivity implements  View.OnClickListener,AdapterClickListener {
    private int idConv;

    private RecyclerView recyclerView;
    private ConversationAdapter mAdapter;
    private List<Message> messArray;
    private DBHelper database;
    private LinearLayoutManager linearLayoutManager;
    private EditText textMessage;
    private MenuItem delete;
    private MenuItem settings;
    private MenuItem clear;
    private MenuItem add;
    private int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_conversation);
        Intent intent = getIntent();
        idConv = intent.getIntExtra(Constants.ID_CONV,0);
        userId = PreferenceManager.getDefaultSharedPreferences(this).getInt(Constants.PREF_USER_ID, 0);

        database = new DBHelper(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle(database.getConvName(idConv, userId));

        textMessage =  findViewById(R.id.inputTextMessage);
        ImageView sendButton = findViewById(R.id.buttonSendMessage);
        sendButton.setOnClickListener(this);

        database.setRead(idConv);
        messArray = database.getMessage(idConv, userId);
        recyclerView = findViewById(R.id.recycler_view_conv);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        RecyclerView.LayoutManager layoutManager = linearLayoutManager;
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new ConversationAdapter(messArray, userId,this);
        mAdapter.sort();
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.buttonSendMessage){
            this.sendMessage();
        }
    }

    public void sendMessage(){
        String message = textMessage.getText().toString();
        if (message.trim().length() > 0) {
            SmsSender.sendSimple(this, idConv, message);
            reloadConv();
            textMessage.getText().clear();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_conv, menu);
        this.delete = menu.findItem(R.id.ic_delete);
        this.add = menu.findItem(R.id.ic_add);
        this.settings = menu.findItem(R.id.action_settings);
        this.clear = menu.findItem(R.id.ic_close);
        delete.setVisible(false);
        clear.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        switch (id){
            case R.id.action_settings :
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.ic_delete :
                final List<Message> toDelete = mAdapter.getSelectedItem();
                new MaterialAlertDialogBuilder(this)
                        .setTitle(getResources().getString(R.string.delete))
                        .setMessage(getResources().getString(R.string.delete_warning))
                        .setPositiveButton(getResources().getString(R.string.validate_delete), (dialogInterface, i) -> {
                            database.delMessage(toDelete);
                            mAdapter.clearSelection();
                            reloadConv();
                        })
                        .setNegativeButton(getResources().getString(R.string.cancel_delete), (dialogInterface, i) -> {
                            mAdapter.clearSelection();
                            reloadConv();
                        })
                        .show();
                return true;
            case R.id.ic_add :
                @SuppressLint("InflateParams") final RelativeLayout relativeLayout = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.user_toconv_alert_layout, null);
                new MaterialAlertDialogBuilder(this)
                        .setView(relativeLayout)
                        .setPositiveButton(getResources().getString(R.string.validate_delete), (dialogInterface, i) -> addUserToConv(((EditText) relativeLayout.findViewById(R.id.user_phone)).getText().toString()))
                        .setNegativeButton(getResources().getString(R.string.cancel_delete), (dialogInterface, i) -> {
                        })
                        .show();
                return true;

            case R.id.ic_close :
                mAdapter.clearSelection();
                reloadConv();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addUserToConv(String phoneNumber){
        SmsSender.requestAddUserConv(this,idConv,phoneNumber);
    }

    public void reloadConv() {
        database.getMessage(idConv, messArray, userId);
        database.setRead(idConv);
        mAdapter.sort();
        mAdapter.notifyDataSetChanged();
        recyclerView.post(() -> linearLayoutManager.smoothScrollToPosition(recyclerView, new RecyclerView.State(), 0));
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle(database.getConvName(idConv, userId));
    }

    @Override
    public void onResume() {
        registerReceiver(mUpdateReceiver, new IntentFilter(Constants.INTENT_SMS));
        reloadConv();
        super.onResume();
    }

    @Override
    public void onPause() {
        unregisterReceiver(mUpdateReceiver);
        super.onPause();
    }

    private BroadcastReceiver mUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            reloadConv();
        }
    };

    @Override
    public void selection(boolean pending) {
        delete.setVisible(pending);
        clear.setVisible(pending);
        settings.setVisible(!pending);
        add.setVisible(!pending);
        if(pending)
            Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.DarkOrange)));
        else
            Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
    }

    @Override
    public void itemClicked(Message m) {

    }
}
