package com.lifprojet.messagetopi.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import com.lifprojet.messagetopi.R;
import com.lifprojet.messagetopi.constants.Constants;
import com.lifprojet.messagetopi.utils.MainSettingsFragment;

import java.util.Objects;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle(getString(R.string.settings));
        setContentView(R.layout.settings_layout);
        MainSettingsFragment settingsFragment = new MainSettingsFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.settings_layout,settingsFragment).commit();
        int userId = PreferenceManager.getDefaultSharedPreferences(this).getInt(Constants.PREF_USER_ID, 0);
        String phone_number = PreferenceManager.getDefaultSharedPreferences(this).getString(Constants.PREF_CLIENT_PHONE_NUMBER, "");
        String nickName = PreferenceManager.getDefaultSharedPreferences(this).getString(Constants.PREF_NICKNAME, "");
        String server_phone = PreferenceManager.getDefaultSharedPreferences(this).getString(Constants.PREF_SERVER_PHONE_NUMBER, "");
        if(!server_phone.equals(""))
            settingsFragment.setServerPhoneNumber(server_phone);
        if(!phone_number.equals(""))
            settingsFragment.setPhoneNumber(phone_number);
        if(!nickName.equals(""))
            settingsFragment.setNickname(nickName);
        if(userId >= 0)
            settingsFragment.setUserId(userId);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
