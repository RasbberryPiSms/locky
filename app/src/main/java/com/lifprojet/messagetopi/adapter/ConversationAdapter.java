package com.lifprojet.messagetopi.adapter;

import android.graphics.Color;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lifprojet.messagetopi.R;
import com.lifprojet.messagetopi.constants.Constants;
import com.lifprojet.messagetopi.obj.Message;
import com.lifprojet.messagetopi.utils.StringToColor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ConversationAdapter extends RecyclerView.Adapter<ConversationAdapter.MyViewHolder> {
    private List<Message> messList;
    private int userId;
    private List<Integer> selectedPos;
    private AdapterClickListener adapterClickListener;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnLongClickListener{
        // each data item is just a string in this case
        public TextView content;
        TextView userName;
        TextView dateMessage;
        ImageView profilePicImage;
        TextView profilePicText;

        MyViewHolder(View view) {
            super(view);
            content = view.findViewById(R.id.message);
            userName = view.findViewById(R.id.name_user_message);
            dateMessage = view.findViewById(R.id.date_message);
            profilePicImage = view.findViewById(R.id.image_profile);
            profilePicText = view.findViewById(R.id.text_profile);
            content.setOnClickListener(this);
            content.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View view) {
            if(selectedPos.isEmpty()) {
                clickAllow();
                notifyItemChanged(getAdapterPosition());
                adapterClickListener.selection(true);
            }
            return true;
        }

        @Override
        public void onClick(View view) {
            if (getAdapterPosition() == RecyclerView.NO_POSITION) return;
            if(!selectedPos.isEmpty()) {
                clickAllow();
                notifyItemChanged(getAdapterPosition());
            }
        }

        private void clickAllow(){
            if (selectedPos.contains(getAdapterPosition())) {
                selectedPos.remove(Integer.valueOf(getAdapterPosition()));
                if(selectedPos.isEmpty())  adapterClickListener.selection(false);
            } else
                selectedPos.add(getAdapterPosition());
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ConversationAdapter(List<Message> messList, int userId, AdapterClickListener adapterClickListener) {
        this.messList = messList;
        this.userId = userId;
        this.selectedPos = new ArrayList<>();
        this.adapterClickListener = adapterClickListener;
    }

    public void sort(){
        Collections.sort(messList, (o1, o2) -> o2.getDate().compareTo(o1.getDate()));
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public ConversationAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        if (viewType == 0) {
            return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_sms_layout_author, parent, false));
        }
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_sms_layout, parent, false));
    }

    @Override
    public int getItemViewType(int position) {
        if (messList.get(position).getIdSender() == userId)
            return 0;
        else if(messList.get(position).getIdSender() == Constants.ID_SERVER)
            return 1;
        return 2;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.content.setText(messList.get(position).getContent());
        holder.content.setBackgroundResource(selectedPos.contains(position) ? R.drawable.selected_rounded_background : R.drawable.rounded_background);
        //Display the name only if we are in a group conv and if the message before isn't from the same idSender
        if(getItemViewType(position) == 2) {
            if (selectedPos.contains(position)) {
                holder.profilePicImage.clearColorFilter();
                holder.profilePicImage.setImageResource(R.drawable.ic_account_selected_54dp);
                holder.profilePicText.setVisibility(View.GONE);
            } else {
                holder.profilePicImage.setColorFilter(Color.parseColor(StringToColor.getColorFar(messList.get(position).getNameSender(), holder.profilePicText.getCurrentTextColor())));
                holder.profilePicImage.setImageResource(R.drawable.ic_account_circle_black_54dp);
                holder.profilePicText.setVisibility(View.VISIBLE);
                holder.profilePicText.setText(messList.get(position).getNameSender());
            }

            if (messList.get(position).isGroup() && (position == messList.size() - 1 || messList.get(position + 1).getIdSender() != messList.get(position).getIdSender())) {
                holder.userName.setVisibility(View.VISIBLE);
                holder.userName.setText(messList.get(position).getNameSender());
            } else
                holder.userName.setVisibility(View.GONE);
        } else if(getItemViewType(position) == 1){
            if (selectedPos.contains(position)) {
                holder.profilePicImage.setImageResource(R.drawable.ic_account_selected_54dp);
            } else {
                holder.profilePicImage.setColorFilter(Color.parseColor("#000000"));
                holder.profilePicImage.setImageResource(R.drawable.server_icon_54dp);
            }
            holder.profilePicText.setVisibility(View.GONE);
            holder.userName.setVisibility(View.GONE);
        }

        if ((position == messList.size() - 1 || messList.get(position).getDate().getTime() - messList.get(position + 1).getDate().getTime() > Constants.MESSAGE_DISPLAY_TIME_INTERVAL)) {
            SimpleDateFormat sdf;
            if(DateUtils.isToday(messList.get(position).getDate().getTime()))
                sdf = new SimpleDateFormat("HH:mm");
            else
                sdf = new SimpleDateFormat("MMM dd - HH:mm");
            holder.dateMessage.setText(sdf.format(messList.get(position).getDate()));
            holder.dateMessage.setVisibility(View.VISIBLE);
        } else {
            holder.dateMessage.setVisibility(View.GONE);
        }
    }

    public List<Message> getSelectedItem(){
        List<Message> messListSelect = new ArrayList<>();
        for(int i : selectedPos){
            messListSelect.add(messList.get(i));
        }
        return messListSelect;
    }

    public void clearSelection(){
        selectedPos.clear();
        adapterClickListener.selection(false);
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return (messList == null) ? 0 : messList.size();
    }
}
