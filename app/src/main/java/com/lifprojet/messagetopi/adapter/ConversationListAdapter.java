package com.lifprojet.messagetopi.adapter;

import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.lifprojet.messagetopi.obj.Message;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;

import android.text.format.DateUtils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lifprojet.messagetopi.R;
import com.lifprojet.messagetopi.utils.StringToColor;

public class ConversationListAdapter extends RecyclerView.Adapter<ConversationListAdapter.MyViewHolder> {
    private List<Message> messList;
    private List<Integer> selectedPos;
    private AdapterClickListener adapterClickListener;
    //private Context context;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnLongClickListener{
        // each data item is just a string in this case
        public TextView content;
        TextView nameConv;
        TextView date;
        ImageView profilePicImage;
        TextView profilePicText;

        MyViewHolder(View view) {
            super(view);
            nameConv = view.findViewById(R.id.name_conv);
            content = view.findViewById(R.id.message);
            date = view.findViewById(R.id.date_last);
            profilePicImage = view.findViewById(R.id.image_profile);
            profilePicText = view.findViewById(R.id.text_profile);
            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View view) {
            if(selectedPos.isEmpty()) {
                clickAllow();
                notifyItemChanged(getAdapterPosition());
                adapterClickListener.selection(true);
            }
            return true;
        }

        @Override
        public void onClick(View view) {
            if (getAdapterPosition() == RecyclerView.NO_POSITION) return;
            if(!selectedPos.isEmpty()) {
                clickAllow();
                notifyItemChanged(getAdapterPosition());
            } else {
                adapterClickListener.itemClicked(messList.get(getAdapterPosition()));
            }
        }

        private void clickAllow(){
            if (selectedPos.contains(getAdapterPosition())) {
                selectedPos.remove(Integer.valueOf(getAdapterPosition()));
                if(selectedPos.isEmpty())  adapterClickListener.selection(false);
            } else
                selectedPos.add(getAdapterPosition());
        }
    }

    // Provide a suitable constructor (depends on the kind of data set)
    public ConversationListAdapter(List<Message> messList, AdapterClickListener adapterClickListener) {
        this.messList = messList;
        this.adapterClickListener = adapterClickListener;
        this.selectedPos = new ArrayList<>();
    }

    public void sort(){
        Collections.sort(messList, (o1, o2) -> o2.getDate().compareTo(o1.getDate()));
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public ConversationListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        //LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.conversation_preview_layout, parent, false);
        return new MyViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your data set at this position
        // - replace the contents of the view with that element
        holder.nameConv.setText(messList.get(position).getNameSender());
        holder.content.setText(messList.get(position).getContent());
        holder.content.setTypeface(null, messList.get(position).isRead() ? Typeface.NORMAL : Typeface.BOLD);

        if(selectedPos.contains(position)) {
            holder.profilePicImage.clearColorFilter();
            holder.profilePicImage.setImageResource(R.drawable.ic_account_selected_54dp);
            holder.profilePicText.setVisibility(View.GONE);
        }else if(messList.get(position).isGroup()) {
            holder.profilePicImage.setImageResource(R.drawable.ic_group_54dp);
            if(messList.get(position).getNameSender() != null)
                holder.profilePicImage.setColorFilter(Color.parseColor(StringToColor.getColorFar(messList.get(position).getNameSender(), holder.profilePicText.getCurrentTextColor())));
            holder.profilePicText.setVisibility(View.GONE);
        } else {
            holder.profilePicImage.setImageResource(R.drawable.ic_account_circle_black_54dp);
            if(messList.get(position).getNameSender() != null)
                holder.profilePicImage.setColorFilter(Color.parseColor(StringToColor.getColorFar(messList.get(position).getNameSender(), holder.profilePicText.getCurrentTextColor())));
            holder.profilePicText.setVisibility(View.VISIBLE);
            holder.profilePicText.setText(messList.get(position).getNameSender());
        }

        SimpleDateFormat sdf;
        if(DateUtils.isToday(messList.get(position).getDate().getTime()))
            sdf = new SimpleDateFormat("HH:mm");
         else
            sdf = new SimpleDateFormat("MMM dd");
        sdf.setTimeZone(TimeZone.getDefault());
        holder.date.setText(sdf.format(messList.get(position).getDate()));
    }

    public List<Message> getSelectedItem(){
        List<Message> messListSelect = new ArrayList<>();
        for(int i : selectedPos){
            messListSelect.add(messList.get(i));
        }
        return messListSelect;
    }

    public void clearSelection(){
        selectedPos.clear();
        adapterClickListener.selection(false);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return (messList == null) ? 0 : messList.size();
    }

}
