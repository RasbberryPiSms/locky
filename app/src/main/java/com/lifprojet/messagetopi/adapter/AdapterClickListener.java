package com.lifprojet.messagetopi.adapter;

import com.lifprojet.messagetopi.obj.Message;

public interface AdapterClickListener {
    void itemClicked(Message idItem);
    void selection(boolean pending);
}
