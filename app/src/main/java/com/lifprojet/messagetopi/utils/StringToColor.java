package com.lifprojet.messagetopi.utils;

import com.lifprojet.messagetopi.constants.Constants;

/**
 * Static class to create color according to string
 *
 * @author Theo REY-VIVIANT
 */
public class StringToColor {

    /**
     * Color in RBG
     *
     * @param stringColor color
     * @return color according to the string in parameter
     */
    public static String getColor(String stringColor){
        int i = stringColor.hashCode();
        return '#' + String.format("%02X", (0xFF & (i>>16))) +
                String.format("%02X", (0xFF & (i>>8))) +
                String.format("%02X", (0xFF & i));
    }

    /**
     * Color in ARGB
     *
     * @param stringColor color
     * @return color according to the string in parameter
     */
    public static String getColorARGB(String stringColor){
        int i = stringColor.hashCode();
        return '#' + String.format("%02X", (0xFF & (i>>24))) +
                String.format("%02X", (0xFF & (i>>16))) +
                String.format("%02X", (0xFF & (i>>8))) +
                String.format("%02X", (0xFF & i));
    }

    /**
     * Color in RGB
     * If the color distance isn't generated in COLOR_REGENERATE_LIMIT
     * Color is set at white
     *
     * @param stringColor color
     * @param red red
     * @param green green
     * @param blue blue
     * @return color with distance from Constants.FAR_COLOR_THRESHOLD
     */
    public static String getColorFar(String stringColor, int red, int green, int blue){
        int i = stringColor.hashCode();
        int newRed = ((i>>16)&0xFF);
        int newGreen = ((i>>8)&0xFF);
        int newBlue = (i&0xFF);
        if(Math.abs((0.2126*newRed + 0.7152*newGreen + 0.0722*newBlue) - (0.2126*red + 0.7152*green + 0.0722*blue)) < Constants.FAR_COLOR_THRESHOLD) {
            if(newRed + Constants.FAR_COLOR_THRESHOLD < 255) newRed += Constants.FAR_COLOR_THRESHOLD;
            if(newGreen + Constants.FAR_COLOR_THRESHOLD < 255) newGreen += Constants.FAR_COLOR_THRESHOLD;
            if(newBlue + Constants.FAR_COLOR_THRESHOLD < 255) newBlue += Constants.FAR_COLOR_THRESHOLD;
        }
        return '#' + String.format("%02X", newRed) +
                String.format("%02X", newGreen) +
                String.format("%02X", newBlue);
    }

    /**
     * Use the int representation of the color
     *
     * @param stringColor color
     * @param color rgb
     * @return value from getColorFar
     */
    public static String getColorFar(String stringColor, int color){
        return getColorFar(stringColor, (int) Long.parseLong(Integer.toHexString(color).substring(2).substring(0,2),16),
                (int) Long.parseLong(Integer.toHexString(color).substring(2).substring(2,4),16),
                (int) Long.parseLong(Integer.toHexString(color).substring(2).substring(4,6),16));
    }
}
