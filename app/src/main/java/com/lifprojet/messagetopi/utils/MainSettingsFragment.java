package com.lifprojet.messagetopi.utils;

import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;

import androidx.preference.EditTextPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.lifprojet.messagetopi.R;
import com.lifprojet.messagetopi.constants.Constants;

import java.util.Objects;

public class MainSettingsFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        // Load the preferences from an XML resource
        setPreferencesFromResource(R.xml.main_preferences, rootKey);
    }

    public void setUserId(int UserId) {
        getParentFragmentManager().executePendingTransactions();
        Preference userID = getPreferenceManager().findPreference(Constants.PREF_USER_ID);
        if(userID != null){
            userID.setSummary(UserId + "");
        }
    }

    public void setPhoneNumber(String phoneNumber) {
        getParentFragmentManager().executePendingTransactions();
        Preference prefPhone = getPreferenceManager().findPreference(Constants.PREF_CLIENT_PHONE_NUMBER);
        if(prefPhone != null){
            prefPhone.setSummary(phoneNumber);
        }
    }

    public void setServerPhoneNumber(String phoneNumber) {
        getParentFragmentManager().executePendingTransactions();
        Preference prefPhone = getPreferenceManager().findPreference(Constants.PREF_SERVER_PHONE_NUMBER);
        if(prefPhone != null){
            prefPhone.setSummary(phoneNumber);
        }
    }

    public void setNickname(String nickname) {
        getParentFragmentManager().executePendingTransactions();
        Preference prefPhone = getPreferenceManager().findPreference(Constants.PREF_NICKNAME);
        if(prefPhone != null){
            prefPhone.setSummary(nickname);
        }
    }
}
