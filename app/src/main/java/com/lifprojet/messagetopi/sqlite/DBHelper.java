package com.lifprojet.messagetopi.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.lifprojet.messagetopi.constants.Constants;
import com.lifprojet.messagetopi.obj.Message;
import com.lifprojet.messagetopi.obj.MessageFrament;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * DBHelper is a class to simplify the user of the sqlite database, it handle all the insert, delete...
 * @author RaspiThim
 */
public class DBHelper extends SQLiteOpenHelper {

    private final String TABLE_WITH_NAME = "( SELECT " + Constants.MESSAGE_TABLE + ".*," + Constants.PROFILE_TABLE + "." + Constants.NAME_SENDER + ", " + Constants.PROFILE_TABLE + "." + Constants.PHONE_SENDER +
            " FROM " + Constants.MESSAGE_TABLE + " LEFT OUTER JOIN " + Constants.PROFILE_TABLE +
            " ON " + Constants.MESSAGE_TABLE + "." + Constants.ID_SENDER + " = " + Constants.PROFILE_TABLE + "." + Constants.ID_SENDER + " )";

    public DBHelper(Context context) {
        super(context, Constants.DATABASE_NAME, null, 14);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE " + Constants.MESSAGE_TABLE + "(" +
                        Constants.ID_MESSAGE + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        Constants.ID_CONV + " INTEGER NOT NULL, " +
                        Constants.ID_SENDER + " INTEGER NOT NULL, " +
                        Constants.TIMESTAMP + " INTEGER NOT NULL, " +
                        Constants.MESSAGE_ISREAD + " BOOLEAN DEFAULT(0), " +
                        Constants.MESSAGE_CONTENT + " TEXT)"
        );
        db.execSQL(
                "CREATE TABLE " + Constants.MESSAGE_FRAGMENT_CONTENT_TABLE + "(" +
                        Constants.ID_MESSAGE + " INTEGER NOT NULL, " +
                        Constants.ID_FRAGMENT + " INTEGER NOT NULL, " +
                        Constants.MESSAGE_CONTENT + " TEXT ," +
                        Constants.FRAGMENT_ISLAST + " BOOLEAN DEFAULT(0), " +
                        "PRIMARY KEY (" + Constants.ID_MESSAGE + ", " + Constants.ID_FRAGMENT + "))"
        );
        db.execSQL(
                "CREATE TABLE " + Constants.MESSAGE_FRAGMENT_HEADER_TABLE + "(" +
                        Constants.ID_MESSAGE + " INTEGER PRIMARY KEY, " +
                        Constants.ID_CONV + " INTEGER NOT NULL, " +
                        Constants.ID_SENDER + " INTEGER NOT NULL, " +
                        Constants.TIMESTAMP + " INTEGER NOT NULL)"
        );
        db.execSQL(
                "CREATE TABLE " + Constants.PROFILE_TABLE + "(" +
                        Constants.ID_SENDER + " INTEGER NOT NULL PRIMARY KEY, " +
                        Constants.PHONE_SENDER + " INTEGER NOT NULL, " +
                        Constants.NAME_SENDER + " TEXT )"
        );
        db.execSQL(
                "CREATE TABLE " + Constants.USER_CONVERSATION_TABLE + "(" +
                        Constants.ID_SENDER + " INTEGER NOT NULL, " +
                        Constants.ID_CONV + " INTEGER NOT NULL, " +
                        Constants.TIMESTAMP + " INTEGER NOT NULL, " +
                        "PRIMARY KEY (" + Constants.ID_SENDER + ", " + Constants.ID_CONV + "))"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Constants.MESSAGE_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + Constants.PROFILE_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + Constants.MESSAGE_FRAGMENT_HEADER_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + Constants.MESSAGE_FRAGMENT_CONTENT_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + Constants.USER_CONVERSATION_TABLE);
        onCreate(db);
    }

    public void clear(){
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + Constants.MESSAGE_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + Constants.PROFILE_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + Constants.MESSAGE_FRAGMENT_HEADER_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + Constants.MESSAGE_FRAGMENT_CONTENT_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + Constants.USER_CONVERSATION_TABLE);
        onCreate(db);
    }

    public void addMessage(int idConv, int idSender, String content) {
        addMessage(idConv, idSender, content, false);
    }

    public void addMessage(int idConv, int idSender, String content, boolean read) {
        ContentValues insetVal = new ContentValues();
        insetVal.put(Constants.ID_CONV, idConv);
        insetVal.put(Constants.ID_SENDER, idSender);
        insetVal.put(Constants.MESSAGE_CONTENT, content);
        insetVal.put(Constants.MESSAGE_ISREAD, read ? 1 : 0);
        insetVal.put(Constants.TIMESTAMP, (new Date().getTime()));
        this.getWritableDatabase().insert(Constants.MESSAGE_TABLE, null, insetVal);
    }

    public void delMessage(List<Message> messList) {
        for (Message i : messList) this.delMessage(i);
    }

    public void delMessage(Message message) {
        this.getWritableDatabase().delete(Constants.MESSAGE_TABLE,
                Constants.ID_MESSAGE + " = ? ",
                new String[]{Integer.toString(message.getIdMessage())});
    }

    public void delConv(List<Message> messList) {
        for (Message i : messList) this.delConv(i.getIdConv());
    }

    public List<Message> getMessage(int idConv, List<Message> messList, int userId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_WITH_NAME + " WHERE " + Constants.ID_CONV + " = " + idConv, null);
        return cursorToMessageList(cursor, messList, userId);
    }

    public Message getLastMessage(Message oldMessage){
        Message message = new Message(oldMessage.getIdConv(), oldMessage.getDate());
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT m.* FROM (SELECT " + Constants.ID_CONV + ", MAX(" + Constants.TIMESTAMP + ") as MaxTime " +
                "FROM " + TABLE_WITH_NAME + " GROUP BY " + Constants.ID_CONV + ") r INNER JOIN " + TABLE_WITH_NAME + " m ON " +
                "m." + Constants.ID_CONV + " = r." + Constants.ID_CONV + " AND m." + Constants.TIMESTAMP + " = r.MaxTime " +
                " WHERE m." + Constants.ID_CONV + " = " + oldMessage.getIdConv() +
                " GROUP BY m." + Constants.ID_CONV + " LIMIT 1", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            message = cursorToMessage(cursor);
            cursor.moveToNext();
        }
        cursor.close();
        return message;
    }

    public List<Message> getMessage(int idConv, int userId) {
        return getMessage(idConv, new ArrayList<>(), userId);
    }

    private List<Message> cursorToMessageList(Cursor cursor, List<Message> messList, int userId) {
        messList.clear();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            messList.add(cursorToMessage(cursor));
            cursor.moveToNext();
        }
        for (Message m : messList) {
            m.setGroup(isGroupConv(userId, m.getIdConv()));
        }
        cursor.close();
        return messList;
    }

    private Message cursorToMessage(Cursor cursor){
        return new Message(cursor.getInt(cursor.getColumnIndex(Constants.ID_MESSAGE)),
                cursor.getInt(cursor.getColumnIndex(Constants.ID_CONV)),
                cursor.getInt(cursor.getColumnIndex(Constants.ID_SENDER)),
                cursor.getColumnIndex(Constants.PHONE_SENDER) != -1 ? cursor.getString(cursor.getColumnIndex(Constants.PHONE_SENDER)) : null,
                cursor.getColumnIndex(Constants.NAME_SENDER) != -1 ? cursor.getString(cursor.getColumnIndex(Constants.NAME_SENDER)) : null,
                new Date(cursor.getLong(cursor.getColumnIndex(Constants.TIMESTAMP))),
                cursor.getString(cursor.getColumnIndex(Constants.MESSAGE_CONTENT)),
                false,
                cursor.getInt(cursor.getColumnIndex(Constants.MESSAGE_ISREAD)) > 0);
    }

    public void setRead(int idConv) {
        ContentValues insetVal = new ContentValues();
        insetVal.put(Constants.MESSAGE_ISREAD, 1);
        this.getWritableDatabase().update(Constants.MESSAGE_TABLE, insetVal, Constants.ID_CONV + " = ?", new String[]{Integer.toString(idConv)});
    }

    public void addProfile(int idSender, String phoneNumber, String name) {
        if (DatabaseUtils.queryNumEntries(this.getReadableDatabase(), Constants.PROFILE_TABLE, Constants.ID_SENDER + " = ?", new String[]{Integer.toString(idSender)}) > 0) {
            this.editProfile(idSender, phoneNumber, name);
        } else {
            ContentValues insetVal = new ContentValues();
            insetVal.put(Constants.ID_SENDER, idSender);
            insetVal.put(Constants.PHONE_SENDER, phoneNumber);
            insetVal.put(Constants.NAME_SENDER, name);
            this.getWritableDatabase().insert(Constants.PROFILE_TABLE, null, insetVal);
        }
    }

    public boolean isProfileExist(int idSender){
        SQLiteDatabase db = this.getReadableDatabase();
        int count = 0;
        Cursor cursor = db.rawQuery("SELECT * FROM " + Constants.PROFILE_TABLE + " WHERE " + Constants.ID_SENDER + " = " + idSender, null);
        cursor.moveToFirst();
        if (!cursor.isAfterLast())
            count++;
        cursor.close();
        return count > 0;
    }

    public void removeProfile(int idSender) {
        this.getWritableDatabase().delete(Constants.PROFILE_TABLE, Constants.ID_SENDER + " = ?", new String[]{Integer.toString(idSender)});
    }

    public String getProfileName(int idSender) {
        SQLiteDatabase db = this.getReadableDatabase();
        String pname = "";
        Cursor cursor = db.rawQuery("SELECT * FROM " + Constants.PROFILE_TABLE + " WHERE " + Constants.ID_SENDER + " = " + idSender, null);
        cursor.moveToFirst();
        if (!cursor.isAfterLast())
            pname = cursor.getString(cursor.getColumnIndex(Constants.NAME_SENDER));
        cursor.close();
        return pname;
    }

    public String getProfilePhone(int idSender) {
        SQLiteDatabase db = this.getReadableDatabase();
        String pname = "";
        Cursor cursor = db.rawQuery("SELECT * FROM " + Constants.PROFILE_TABLE + " WHERE " + Constants.ID_SENDER + " = " + idSender, null);
        cursor.moveToFirst();
        if (!cursor.isAfterLast())
            pname = cursor.getString(cursor.getColumnIndex(Constants.PHONE_SENDER));
        cursor.close();
        return pname;
    }

    public void editProfile(int idSender, String phoneNumber, String name) {
        ContentValues insetVal = new ContentValues();
        insetVal.put(Constants.PHONE_SENDER, phoneNumber);
        insetVal.put(Constants.NAME_SENDER, name);
        this.getWritableDatabase().update(Constants.PROFILE_TABLE, insetVal, Constants.ID_SENDER + " = ?", new String[]{Integer.toString(idSender)});
    }

    public List<Integer> getProfileId(String nameSender) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Constants.PROFILE_TABLE + " WHERE " + Constants.NAME_SENDER + " LIKE  " + nameSender, null);
        List<Integer> intList = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            intList.add(cursor.getInt(cursor.getColumnIndex(Constants.ID_SENDER)));
            cursor.moveToNext();
        }
        cursor.close();
        return intList;
    }

    public boolean fragmentHeaderExist(long idMessage) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Constants.MESSAGE_FRAGMENT_HEADER_TABLE +
                " WHERE " + Constants.ID_MESSAGE + " = " + idMessage, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Date messDate = new Date(cursor.getLong(cursor.getColumnIndex(Constants.TIMESTAMP)));
            if ((new Date()).getTime() - messDate.getTime() < Constants.MESSAGE_FRAGMENT_TIMEOUT) {
                cursor.close();
                return true;
            } else {
                deleteFragment(idMessage);
            }
            cursor.moveToNext();
        }
        cursor.close();
        return false;
    }

    public int[] getFragmentHeader(long idMessage) {
        SQLiteDatabase db = this.getReadableDatabase();
        int[] result = {-1, -1};
        Cursor cursor = db.rawQuery("SELECT * FROM " + Constants.MESSAGE_FRAGMENT_HEADER_TABLE +
                " WHERE " + Constants.ID_MESSAGE + " = " + idMessage, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            result[0] = cursor.getInt(cursor.getColumnIndex(Constants.ID_CONV));
            result[1] = cursor.getInt(cursor.getColumnIndex(Constants.ID_SENDER));
            cursor.moveToNext();
        }
        cursor.close();
        return result;
    }

    public void deleteFragment(long idMessage) {
        this.getWritableDatabase().delete(Constants.MESSAGE_FRAGMENT_HEADER_TABLE, Constants.ID_MESSAGE + " = ?", new String[]{Long.toString(idMessage)});
        this.getWritableDatabase().delete(Constants.MESSAGE_FRAGMENT_CONTENT_TABLE, Constants.ID_MESSAGE + " = ?", new String[]{Long.toString(idMessage)});
    }

    public void createFragmentHeader(long idMessage, int idConv, int idSender) {
        ContentValues insetVal = new ContentValues();
        insetVal.put(Constants.ID_MESSAGE, idMessage);
        insetVal.put(Constants.ID_CONV, idConv);
        insetVal.put(Constants.ID_SENDER, idSender);
        insetVal.put(Constants.TIMESTAMP, (new Date().getTime()));
        this.getWritableDatabase().insert(Constants.MESSAGE_FRAGMENT_HEADER_TABLE, null, insetVal);
    }

    public void addFragment(long idMessage, long idFragment, boolean isLast, String content) {
        ContentValues insetVal = new ContentValues();
        insetVal.put(Constants.ID_MESSAGE, idMessage);
        insetVal.put(Constants.ID_FRAGMENT, idFragment);
        insetVal.put(Constants.MESSAGE_CONTENT, content);
        insetVal.put(Constants.FRAGMENT_ISLAST, (isLast ? 1 : 0));
        this.getWritableDatabase().insert(Constants.MESSAGE_FRAGMENT_CONTENT_TABLE, null, insetVal);
    }

    public boolean convertFullFragment(long idMessage) {
        List<MessageFrament> fragList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Constants.MESSAGE_FRAGMENT_CONTENT_TABLE +
                " WHERE " + Constants.ID_MESSAGE + " = " + idMessage, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            fragList.add(new MessageFrament(
                    cursor.getInt(cursor.getColumnIndex(Constants.ID_MESSAGE)),
                    cursor.getInt(cursor.getColumnIndex(Constants.ID_FRAGMENT)),
                    cursor.getString(cursor.getColumnIndex(Constants.MESSAGE_CONTENT)),
                    cursor.getInt(cursor.getColumnIndex(Constants.FRAGMENT_ISLAST)) > 0));
            cursor.moveToNext();
        }
        cursor.close();
        for (MessageFrament frag : fragList) {
            if (frag.getNumberFrag() != -1)
                if (fragList.size() == frag.getNumberFrag()) {
                    createMessageFromFragment(fragList);
                    deleteFragment(frag.getIdMessage());
                    return true;
                }
        }
        return false;
    }

    public void createMessageFromFragment(List<MessageFrament> fragList) {
        StringBuilder sumContent = new StringBuilder();
        Collections.sort(fragList);
        for (MessageFrament frag : fragList) sumContent.append(frag.getContent());
        if (fragList.size() > 0) {
            int[] header = this.getFragmentHeader(fragList.get(0).getIdMessage());
            if (header[0] != -1 && header[1] != -1)
                this.addMessage(header[0], header[1], sumContent.toString());
        }
    }

    public void delConv(int idConv) {
        this.getWritableDatabase().delete(Constants.MESSAGE_TABLE,
                Constants.ID_CONV + " = ? ",
                new String[]{Integer.toString(idConv)});
        this.getWritableDatabase().delete(Constants.USER_CONVERSATION_TABLE,
                Constants.ID_CONV + " = ? ",
                new String[]{Integer.toString(idConv)});
    }

    public void removeFromConv(int idConv,int idUser){
        this.getWritableDatabase().delete(Constants.USER_CONVERSATION_TABLE,
                Constants.ID_CONV + " = ? AND " + Constants.ID_SENDER + " = ?",
                new String[]{Integer.toString(idConv), Integer.toString(idUser)});
    }

    public void addUserToConversation(int idConv, int idUser) {
        ContentValues insetVal = new ContentValues();
        insetVal.put(Constants.ID_SENDER, idUser);
        insetVal.put(Constants.ID_CONV, idConv);
        insetVal.put(Constants.TIMESTAMP, (new Date().getTime()));
        this.getWritableDatabase().insert(Constants.USER_CONVERSATION_TABLE, null, insetVal);
    }

    public boolean isGroupConv(int userId, int idConv) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Constants.USER_CONVERSATION_TABLE +
                " WHERE " + Constants.ID_CONV + " = " + idConv, null);
        cursor.moveToFirst();
        int count = 1;
        while (!cursor.isAfterLast()) {
            int currId = cursor.getInt(cursor.getColumnIndex(Constants.ID_SENDER));
            if (currId != userId && currId != Constants.ID_SERVER)
                count++;
            if (count > 2) break;
            cursor.moveToNext();
        }
        cursor.close();
        return count > 2;
    }

    public List<Integer> getUserConv(int idConv) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Constants.USER_CONVERSATION_TABLE +
                " WHERE " + Constants.ID_CONV + " = " + idConv, null);
        List<Integer> userInConv = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            int currId = cursor.getInt(cursor.getColumnIndex(Constants.ID_SENDER));
            if (currId != Constants.ID_SERVER && !userInConv.contains(currId))
                userInConv.add(currId);
            cursor.moveToNext();
        }
        cursor.close();
        return userInConv;
    }

    public String getConvName(int idConv, int userID){
        StringBuilder convName = new StringBuilder();
        for (int i : getUserConv(idConv)) {
            if (i != userID && i != Constants.ID_SERVER)
                convName.append(!this.getProfileName(i).equals("") ? this.getProfileName(i) : (!this.getProfilePhone(i).equals("") ? this.getProfilePhone(i) : String.valueOf(i))).append(", ");
        }
        if (convName.length() > 2)
            convName = new StringBuilder(convName.substring(0, convName.length() - 2));
        return convName.toString();
    }

    public String getPhoneNumber(int idConv, int userID){
        StringBuilder phone = new StringBuilder();
        for (int i : getUserConv(idConv))
            if (i != userID && i != Constants.ID_SERVER)
                phone.append(" ").append(this.getProfilePhone(i));
        return phone.toString();
    }

    public List<Message> getListConvMessage(int userId){
        return getListConvMessage(new ArrayList<>(), userId);
    }

    public List<Message> getListConvMessage(List<Message> messageList, int userId){
        messageList.clear();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Constants.USER_CONVERSATION_TABLE +
                        " WHERE " + Constants.ID_SENDER + " != " + userId +
                        " GROUP BY " + Constants.ID_CONV +
                        " ORDER BY " + Constants.TIMESTAMP + " DESC ", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            messageList.add(new Message(cursor.getInt(cursor.getColumnIndex(Constants.ID_CONV)),
                    new Date(cursor.getLong(cursor.getColumnIndex(Constants.TIMESTAMP)))));
            cursor.moveToNext();
        }
        for(Message message : messageList){
            message.copy(getLastMessage(message));
            message.setGroup(isGroupConv(userId, message.getIdConv()));
            message.setNameSender(getConvName(message.getIdConv(), userId));
            message.setPhoneSender(getPhoneNumber(message.getIdConv(), userId));
        }
        cursor.close();
        return messageList;
    }
}